import { ComponentProps, ReactNode } from 'react';

export type ButtonProps = ComponentProps<'button'> & {
  children: ReactNode;
  color?: 'primary' | 'secondary';
  size?: 'sm' | 'md' | 'lg';
};
