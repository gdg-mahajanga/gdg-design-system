import type { ButtonProps } from './Button.props';
import './Button.css';

export default function GDGButton({
  children,
  color = 'primary',
  size = 'md',
  ...props
}: ButtonProps) {
  return (
    <button
      {...props}
      className={`gdg-button gdg-button--${color} gdg-button--${size} ${props.className}`}
    >
      {children}
    </button>
  );
}
